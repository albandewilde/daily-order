using System.Text.Json;
using System.Threading.Tasks;
using DailyOrder.Models;

var builder = WebApplication.CreateBuilder(args);
var app = builder.Build();

Peoples ps = new("/vol/blifi.json");

app.MapGet("/", () => "Server is up and running !");
app.MapGet("/peoples/", () => ps.ToJson());
app.MapGet("/shuffle/", () => ps.Shuffle().ToJson());
app.MapPost("/people/", ctx => AddPeople(ctx));
app.MapDelete("/people/", ctx => DeletePeople(ctx));

app.Run();

async Task<IResult> AddPeople(HttpContext ctx)
{
    string body = await new StreamReader(ctx.Request.Body).ReadToEndAsync();
    string name = JsonSerializer.Deserialize<string>(body)!;
    ps.Add(name);
    return Results.Ok();
}

async Task<IResult> DeletePeople(HttpContext ctx)
{
    string body = await new StreamReader(ctx.Request.Body).ReadToEndAsync();
    string name = JsonSerializer.Deserialize<string>(body)!;
    ps.Delete(name);
    return Results.Ok();
}
