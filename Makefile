.PHONY: image ctn-run

image:
	@docker build . -t daily_order

ctn-run:
	@docker run \
		--volume ${PWD}:/vol/ \
		-p 5144:80 \
		daily_order
