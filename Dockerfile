FROM mcr.microsoft.com/dotnet/sdk:6.0 as builder

WORKDIR /app

COPY . .

RUN dotnet restore

RUN dotnet publish -c Release -o /app/out


FROM mcr.microsoft.com/dotnet/aspnet:6.0

WORKDIR /app

COPY --from=builder /app/out .

EXPOSE 5144

CMD ["dotnet", "DailyOrder.dll"]
