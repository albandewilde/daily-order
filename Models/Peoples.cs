using System.Text.Json;
using System.Threading;

using People = System.String;

namespace DailyOrder.Models;

public class Peoples
{
    private List<People> _ps;
    private Mutex _mtx;
    public string? FilePath;

    public Peoples(List<People> ps)
    {
        _ps = ps;
        _mtx = new Mutex();
    }

    public Peoples() : this(new List<People>()) {}

    public Peoples(string filepath)
    {
        StreamReader r = new StreamReader(filepath);
        string jsn = r.ReadToEnd();
        List<People> ps = JsonSerializer.Deserialize<List<People>>(jsn)!;

        _ps = ps;
        _mtx = new Mutex();
        FilePath = filepath;
    }

    public void Add(People p)
    {
        _mtx.WaitOne();
        _ps.Add(p);
        _mtx.ReleaseMutex();
        Save();
    }

    public string ToJson()
    {
        _mtx.WaitOne();
        string jsnPs = JsonSerializer.Serialize<List<People>>(_ps);
        _mtx.ReleaseMutex();
        return jsnPs;
    }

    public void Delete(People p)
    {
        _mtx.WaitOne();
        if (!_ps.Contains(p))
        {
            _mtx.ReleaseMutex();
            return;
        }
        _ps.Remove(p);
        _mtx.ReleaseMutex();
        Save();
    }

    public Peoples Shuffle()
    {
        _mtx.WaitOne();
        Peoples newPs = new Peoples(_ps.OrderBy(e => Guid.NewGuid()).ToList());
        _mtx.ReleaseMutex();
        return newPs;
    }

    public void Save()
    {
        if (FilePath is null)
        {
            throw new Exception("No file name given");
        }
        string jsn = ToJson();
        _mtx.WaitOne();
        File.WriteAllText(FilePath, jsn);
        _mtx.ReleaseMutex();
    }
}
